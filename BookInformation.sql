create database Books
use Books
create table tbl_admin
(
ad_id int identity primary key,
ad_username nvarchar(50) not null,
ad_password nvarchar(50) not null
)
insert into tbl_admin values('admin','admin123')
select * from tbl_admin
------------------------------------------
create table tbl_book
(
book_id int identity primary key ,
book_title nvarchar(50) not null,
book_author nvarchar(50) not null,
book_desc nvarchar(max) not null,
book_publish_year int not null,
IsAvailable bit
)
drop table tbl_book
book_fk_author int foreign key references tbl_author(author_id),


select * from tbl_book
select * from tbl_author

alter proc Sp_book_Add
@book_title nvarchar(50),
@book_author nvarchar(50),
@book_desc nvarchar(max),
@book_publish_year int,
@IsAvailable bit
as
begin
Insert into tbl_book values(
@book_title ,
@book_author,
@book_desc ,
@book_publish_year,

@IsAvailable 
)
end
-------------------------------------------
alter proc Sp_book_Update
@book_id int,
@book_title nvarchar(50),
@book_author nvarchar(50),
@book_desc nvarchar(max),
@book_publish_year int,

@IsAvailable bit
as
begin
Update tbl_book set
book_title = @book_title ,
book_author = @book_author,
book_desc =@book_desc ,
book_publish_year=@book_publish_year,

IsAvailable=@IsAvailable 
where book_id=@book_id
end
select * from tbl_book
select * from tbl_author


-----------------------------------
create proc Sp_book_Delete
@book_id int
as
begin
Delete from tbl_book where book_id=@book_id
end

------------------------------------------
alter proc Sp_book_Search
@book_id int
as
begin
Select * from tbl_book where book_id=@book_id
end

------------------------------
create proc Sp_book_Show

as
begin
Select * from tbl_book 
end
---------------------
create table tbl_author
(
author_id int identity primary key ,
author_name nvarchar(50) not null,
author_desc nvarchar(max) not null,
author_country nvarchar(50) not null
)
drop table tbl_author

select * from tbl_author





drop table tbl_available
select * from tbl_available

---------------------------------------------------------------
-----------------------------------


alter proc Sp_author_Add
@author_name nvarchar(50),
@author_desc nvarchar(max),
@author_country nvarchar(50)
as
begin
Insert into tbl_author values(
@author_name ,
@author_desc,
@author_country 
)
end
exec Sp_author_Add


-------------------------------------------
alter proc Sp_author_Update
@author_id int,
@author_name nvarchar(50),
@author_country nvarchar(50),
@author_desc nvarchar(max)
as
begin
Update tbl_author set
author_name = @author_name ,
author_country = @author_country,
author_desc =@author_desc 
where author_id=@author_id
end


-----------------------------------
create proc Sp_author_Delete
@author_id int
as
begin
Delete from tbl_author where author_id=@author_id
end

------------------------------------------
create proc Sp_author_Search
@author_id int
as
begin
Select * from tbl_author where author_id=@author_id
end
--------------------------------------------

create proc Sp_author_Show

as
begin
Select * from tbl_author 
end
execute Sp_author_Show
------
create proc Sp_author_Showdis

as
begin

Select distinct author_name,author_desc,author_country from tbl_author 
end

-----------------
-----------------------------
---------------------------





---------------------


select * from tbl_author




create proc Sp_Dashboard

as
begin
Select b.book_title,a.author_name,b.IsAvailable
from tbl_book b inner JOIN tbl_author a 
on b.book_author=a.author_id
end
select * from tbl_book
select * from tbl_author