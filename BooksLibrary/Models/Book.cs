﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BooksLibrary.Models
{
    public class Book
    {
        [Display(Name = "Book Id")]
        public int book_id { get; set; }
        
        [Required(ErrorMessage ="Please enter book title"),MaxLength(30)]
        [Display(Name="Book Title")]
        public string book_title { get; set; }

        [Required(ErrorMessage = "Select author name")]
        [Display(Name = "Author")]
        public string book_author { get; set; }

        [Required(ErrorMessage = "Please enter book description")]
        [Display(Name = "Description Of Book")]
        public string book_desc { get; set; }

        
        [Range(0, int.MaxValue, ErrorMessage = "Please enter published year")]
        [Display(Name = "Published Year")]
        public int book_publish_year { get; set; }

        [Required(ErrorMessage = "*")]
        [Display(Name = "Book Availability")]
        public int IsAvailable { get; set; }
        
        
        public List<SelectListAuthor> _list { get; set; }

        public class SelectListAuthor
        {
            public string author_name { get; set; }
            public int author_id { get; set; }
          

        }


    }
}