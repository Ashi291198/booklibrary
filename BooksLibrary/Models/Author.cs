﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace BooksLibrary.Models
{
    public class Author
    {
        [Display(Name = "Author Id")]
        public int author_id { get; set; }

        [Required(ErrorMessage = "Please enter author name"), MaxLength(30)]
        [Display(Name = "Author")]
        public string author_name { get; set; }

        [Required(ErrorMessage = "Please enter about author")]
        [Display(Name = "About Author")]
        public string author_desc { get; set; }

        [Required(ErrorMessage = "Please enter country to which author belong"), MaxLength(20)]
        [Display(Name = "Country")]
        public  string author_country { get; set; }

       
    }
}