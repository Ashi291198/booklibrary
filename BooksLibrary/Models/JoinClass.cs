﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BooksLibrary.Models
{
    public class JoinClass
    {

        public string book_title { get; set; }
        public string author_name { get; set; }
        public int IsAvailable { get; set; }

    }
}