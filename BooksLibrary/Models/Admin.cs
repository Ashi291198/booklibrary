﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BooksLibrary.Models
{
    public class Admin
    {
        public int ad_id { get; set; }
        [Required(ErrorMessage = "Enter name here")]
        public string ad_username { get; set; }
        [Required(ErrorMessage = "Enter password")]
        public string ad_password { get; set; }
    }
}