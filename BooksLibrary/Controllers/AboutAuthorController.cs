﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Web.Mvc;
using BooksLibrary.Models;

namespace BooksLibrary.Controllers
{
    public class AboutAuthorController : Controller
    {

        string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
        // GET: AboutAuthor
        public ActionResult AddAuthor()
        {
            string HasSession = Convert.ToString(Session["ad_username"]);
            if (string.IsNullOrEmpty(HasSession))
            {
                return RedirectToAction("Index_fe", "Home");
            }
            return View();
        }
        [HttpPost]
        public ActionResult AddAuthor(Author au)
        {
            try
            {
                SqlConnection conn = new SqlConnection(cs);
                conn.Open();
                SqlCommand cmd = new SqlCommand("Sp_author_Add", conn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@author_name", au.author_name);
                cmd.Parameters.AddWithValue("@author_country", au.author_country);
                cmd.Parameters.AddWithValue("@author_desc", au.author_desc);
                cmd.ExecuteNonQuery();
                ViewBag.msg = "Record Added Successfully !";
                conn.Close();
                ModelState.Clear();
            }
            catch (Exception ex)
            {
            }
            return View();
        }

        public ActionResult AShowData(Author au)
        {
            string HasSession = Convert.ToString(Session["ad_username"]);
            if (string.IsNullOrEmpty(HasSession))
            {
                return RedirectToAction("Index_fe", "Home");
            }
            SqlConnection conn = new SqlConnection(cs);
            conn.Open();
            SqlDataAdapter cmd = new SqlDataAdapter("Sp_author_Show", conn);
            DataTable dt = new DataTable();
            List<Author> list = new List<Author>();
            cmd.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    list.Add(new Author
                    {
                        author_id = Convert.ToInt32(dr["author_id"]),
                        author_name = dr["author_name"].ToString(),
                        author_country = dr["author_country"].ToString(),
                        author_desc = dr["author_desc"].ToString()
                   }); ;
                }
            }
            conn.Close();
            return View(list);
        }

        public ActionResult Delete(int? id)
        {
            string HasSession = Convert.ToString(Session["ad_username"]);
            if (string.IsNullOrEmpty(HasSession))
            {
                return RedirectToAction("Index_fe", "Home");
            }
            SqlConnection conn = new SqlConnection(cs);

            SqlCommand cmd = new SqlCommand("Sp_author_Delete", conn);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@author_id", id);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            ViewBag.msg = "Record has been deleted Successfully with bookid" + " " + id;

            return RedirectToAction("AShowData");
        }

        public ActionResult AEdit(int? id)
        {
            string HasSession = Convert.ToString(Session["ad_username"]);
            if (string.IsNullOrEmpty(HasSession))
            {
                return RedirectToAction("Index_fe", "Home");
            }
            Author au = new Author();
            SqlConnection conn = new SqlConnection(cs);
            conn.Open();
            SqlCommand cmd = new SqlCommand("Sp_author_Search", conn);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@author_id", id);
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                au.author_id = Convert.ToInt32(dr["author_id"]);
                au.author_name = dr["author_name"].ToString();
                au.author_country = dr["author_country"].ToString();
                au.author_desc = dr["author_desc"].ToString();
            }
            conn.Close();
            return View(au);
        }
        [HttpPost]
        public ActionResult AEdit(Author au)
        {

            SqlConnection conn = new SqlConnection(cs);
            conn.Open();
            SqlCommand cmd = new SqlCommand("Sp_author_Update", conn);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@author_id", au.author_id);
            cmd.Parameters.AddWithValue("@author_name", au.author_name);
            cmd.Parameters.AddWithValue("@author_country", au.author_country);
            cmd.Parameters.AddWithValue("@author_desc", au.author_desc);
            cmd.ExecuteNonQuery();
            ViewBag.msg = "Updated Successfully!";
            conn.Close();
            return RedirectToAction("AShowData");
        }


    }
}