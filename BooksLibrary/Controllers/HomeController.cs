﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using BooksLibrary.Models;

namespace BooksLibrary.Controllers
{
    public class HomeController : Controller
    {
        string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
        public ActionResult Index_fe()
        {
            return View();
        }

        public ActionResult Book_fe(JoinClass jc)
        {
            SqlConnection conn = new SqlConnection(cs);
            conn.Open();
            SqlDataAdapter cmd = new SqlDataAdapter("Sp_Dashboard", conn);
            DataTable dt = new DataTable();
            List<JoinClass> list = new List<JoinClass>();


            cmd.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    list.Add(new JoinClass
                    {
                       
                        book_title = dr["book_title"].ToString(),
                        author_name = dr["author_name"].ToString(),
                        IsAvailable = Convert.ToInt32(dr["IsAvailable"])
                       

                    }); ;


                }
            }
            conn.Close();
            return View(list);
        }


        public ActionResult Author_fe(Author au)
        {

            SqlConnection conn = new SqlConnection(cs);
            conn.Open();
            SqlDataAdapter cmd = new SqlDataAdapter("Sp_author_Show", conn);
            DataTable dt = new DataTable();
            List<Author> list = new List<Author>();


            cmd.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    list.Add(new Author
                    {

                        author_name = dr["author_name"].ToString(),
                        author_country= dr["author_country"].ToString(),
                        author_desc = dr["author_desc"].ToString(),


                    }); ;


                }
            }
            conn.Close();
            return View(list);
        }

        public ActionResult About_fe()
        {
            return View();
        }
        public ActionResult Contact_fe()
        {
            return View();
        }
       


    }



}
