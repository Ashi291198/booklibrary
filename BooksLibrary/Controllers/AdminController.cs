﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using BooksLibrary.Models;

namespace BooksLibrary.Controllers
{
    public class AdminController : Controller
    {
        string con = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(Admin v)
        {
            string Query = "";
            Query += "select * from tbl_admin where ad_username='" + v.ad_username + "' and ad_password='" + v.ad_password + "'";
            SqlDataAdapter cmd = new SqlDataAdapter(Query, con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                Session["ad_username"] = dt.Rows[0]["ad_username"];
                
                return RedirectToAction("AddBook","AddBook");
            }
            else {
                ViewBag.msg = "Invalid Username or Password";

            }
            return View();
        }
        public ActionResult Dashboard()
        {
            if (Session["ad_username"] == null)
            {
                ViewBag.msg = "You are not logged in..Login First! ";
                return RedirectToAction("Index");

            }
            return View();
        }

        public ActionResult LogOut()
        {
            Session.Abandon();
            Session.Clear();
            return RedirectToAction("Index_fe","Home");
        }
    }
}