﻿using BooksLibrary.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static BooksLibrary.Models.Book;

namespace BooksLibrary.Controllers
{
    public class AddBookController : Controller
    {
        string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
        // GET: AddBook
        public ActionResult AddBook()
        {
            string HasSession = Convert.ToString(Session["ad_username"]);
            if (string.IsNullOrEmpty(HasSession))
            {
                return RedirectToAction("Index_fe", "Home");
            }
            var GetList = GetAuthorlist().ToList();
                ViewBag.Author = (from auth in GetAuthorlist() select new SelectListAuthor { author_id = auth.author_id, author_name = auth.author_name }).ToList();
                return View();
            
          
        }


        
        public List<SelectListAuthor> GetAuthorlist()
        {
            List<SelectListAuthor> _list = new List<SelectListAuthor>();
            string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            SqlConnection conn = new SqlConnection(cs);
            {
                string query = " Select author_id, author_name from tbl_author";
              
                SqlCommand cmd = new SqlCommand(query, conn);
                conn.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            _list.Add(new SelectListAuthor
                            {
                                author_id = Convert.ToInt32(sdr["author_id"]),
                                author_name = sdr["author_name"].ToString()
                            });
                        }
                    }
                    conn.Close();
                }
             return _list;
        }

        [HttpPost]
        public ActionResult AddBook(Book b)
        {
            if (ModelState.IsValid)
            {
                SqlConnection conn = new SqlConnection(cs);
                conn.Open();
                SqlCommand cmd = new SqlCommand("Sp_book_Add", conn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@book_title", b.book_title);
                cmd.Parameters.AddWithValue("@book_author", b.book_author);
                cmd.Parameters.AddWithValue("@book_desc", b.book_desc);
                cmd.Parameters.AddWithValue("@book_publish_year", b.book_publish_year);
                cmd.Parameters.AddWithValue("@IsAvailable", b.IsAvailable);
                cmd.ExecuteNonQuery();
                ViewBag.msg = "Record Added Successfully";
                conn.Close();
                ModelState.Clear();

            }
            ViewBag.Author = (from auth in GetAuthorlist() select new SelectListAuthor { author_id = auth.author_id, author_name = auth.author_name }).ToList();
            return View();
        }
        

        public ActionResult ShowData(Book b)
        {
            string HasSession = Convert.ToString(Session["ad_username"]);
            if (string.IsNullOrEmpty(HasSession))
            {
                return RedirectToAction("Index_fe", "Home");
            }
            SqlConnection conn = new SqlConnection(cs);
            conn.Open();
            SqlDataAdapter cmd = new SqlDataAdapter("Sp_book_Show", conn);
            DataTable dt = new DataTable();
            List<Book> list = new List<Book>();
            cmd.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    list.Add(new Book
                    {
                        book_id = Convert.ToInt32(dr["book_id"]), 
                        book_title = dr["book_title"].ToString(),
                        book_author = dr["book_author"].ToString(),
                        book_desc = dr["book_desc"].ToString(),
                        book_publish_year = Convert.ToInt32(dr["book_publish_year"]),
                        IsAvailable = Convert.ToInt32(dr["IsAvailable"])

                    }); ;
                }
            }
            conn.Close();
            return View(list);
        }

        public ActionResult Delete(int? id)
        {
            string HasSession = Convert.ToString(Session["ad_username"]);
            if (string.IsNullOrEmpty(HasSession))
            {
                return RedirectToAction("Index_fe", "Home");
            }
            SqlConnection conn = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("Sp_book_Delete", conn);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@book_id", id);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            ViewBag.msg = "Record has been deleted Successfully with bookid"+" "+ id;
            return RedirectToAction("ShowData");
        }

        public ActionResult Edit(int? id)
        {
            string HasSession = Convert.ToString(Session["ad_username"]);
            if (string.IsNullOrEmpty(HasSession))
            {
                return RedirectToAction("Index_fe", "Home");
            }

            
            Book b = new Book();
            SqlConnection conn = new SqlConnection(cs);
            conn.Open();
            SqlCommand cmd = new SqlCommand("Sp_book_Search", conn);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@book_id", id);
            SqlDataReader dr = cmd.ExecuteReader();
            while(dr.Read())
            {
                b.book_id = Convert.ToInt32(dr["book_id"]);
                b.book_title = dr["book_title"].ToString();
                b.book_author = dr["book_author"].ToString();
                b.book_desc = dr["book_desc"].ToString();
                b.book_publish_year = Convert.ToInt32(dr["book_publish_year"]);
                b.IsAvailable = Convert.ToInt32(dr["IsAvailable"]);

            }
            conn.Close();
            ViewBag.Author = new SelectList(GetAuthorlist(), "author_id", "author_name", b.book_author.ToString());
            return View(b);
        }
        [HttpPost]
        public ActionResult Edit(Book b)
        {
            SqlConnection conn = new SqlConnection(cs);
            conn.Open();
            SqlCommand cmd = new SqlCommand("Sp_book_Update", conn);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@book_id", b.book_id);
            cmd.Parameters.AddWithValue("@book_title", b.book_title);
            cmd.Parameters.AddWithValue("@book_author", b.book_author);
            cmd.Parameters.AddWithValue("@book_desc", b.book_desc);
            cmd.Parameters.AddWithValue("@book_publish_year", b.book_publish_year);
            cmd.Parameters.AddWithValue("@IsAvailable", b.IsAvailable);

            cmd.ExecuteNonQuery();
            ViewBag.msg = "Updated Successfully!";
            conn.Close();
            ModelState.Clear();
            ViewBag.Author = new SelectList(GetAuthorlist(), "author_id", "author_name", b.book_author.ToString());
            return View("Edit");
        }

    }
}